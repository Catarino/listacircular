/*

UNION

GALERA DE EDA COM O FERNANDO
Segue os enunciados do diario de bordo de hoje. Infelizmente havia copiado so pra mim e ja tinha terminado o exercicio 1 entao so tenho o enunciado do 2 e 3, mas espero que ajude!
2) alterar o codigo anterior p/ realizar as seguintes operacoes
*LI-> lista os elementos em ordem inversa
*Remover elementos da lista em posicoes aleatorias
*Alterar elementos da lista
3) Uma lista circular duplamente encadeada possui elementos que representam somas geometricas variadas: cilindros, cubos e piramides. Monte um programa que leia entradas com:
i) O tipo de figura geometrica
CL-> cilindro
CB-> cubo
PI-> piramide
ii) Informacoes necessarias p/ o calculo do volume da forma geometrica.
OBS: Cada forma possui informacoes diferente entre si.
iii) TIpo de operacao a ser feita na lista
a) I-> inclusao do elemento
b) R-> remocao do elemento
OBS: Incluir funcoes para: Imprimir a lista geral e o volume geral e imprimir a lista por tipo de forma geometrica e o volume total de objeto.
Descurtir � 
*/
#include<stdio.h>
#include<stdlib.h>

typedef struct Tno{
	void *p;
	Tno *prox;
	Tno *ant;
}Tno;

typedef struct Cilindro{
	int raio;
	int altura;
	Cilindro *prox;
	Cilindro *ant;
}Cilindro;

typedef struct Cubo{
	int base;
	Cubo *prox;
	Cubo *ant;
}Cubo;

typedef struct Piramide{
	int base;
	int altura;
	Piramide *prox;
	Piramide *ant;
}Piramide;

typedef struct Cabecalho{
	int nElementos_Piramide;
	int nElementos_Cilindro;
	int nElementos_Cubo;
	Cilindro *head_Cilindro;
	Cilindro *tail_Cilindro;
	Cubo *head_Cubo;
	Cubo *tail_Cubo;
	Piramide *head_Piramide;
	Piramide *tail_Piramide;
}Cabecalho;

void incluiCilindro(Tno *pLista, Cabecalho *L, int raio, int altura){
	Tno *pAux;
	Cilindro *pNovoNo;
	pNovoNo = (Cilindro *)malloc(sizeof(Cilindro));
	pAux = pLista;
	pNovoNo->raio = raio;
	pNovoNo->altura = altura;
	if(pAux == NULL){
		pNovoNo->prox = pNovoNo;
		pNovoNo->ant = pNovoNo;
		pAux->p = pNovoNo;
	}
}

int main(){
	Tno *pLista;
	Cabecalho *L;
	pLista = (Tno *)malloc(sizeof(Tno));
	L = (Cabecalho *)malloc(sizeof(Cabecalho));
	pLista->prox = NULL;
	pLista->ant = NULL;
	L->head_Cilindro = NULL;
	L->tail_Cilindro = NULL;
	L->nElementos_Cilindro = 0;
	L->head_Cubo = NULL;
	L->tail_Cubo = NULL;
	L->nElementos_Cubo = 0;	
	L->head_Piramide = NULL;
	L->tail_Piramide = NULL;
	L->nElementos_Piramide = 0;
	return 0;
}

