/*
		2) alterar o codigo anterior p/ realizar as seguintes operacoes
		*LI-> lista os elementos em ordem inversa
		*Remover elementos da lista em posicoes aleatorias
		*Alterar elementos da lista
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Tno{
	int valor;
	Tno *prox;
	Tno *ant;
}Tno;

typedef struct Cabecalho{
	Tno *head;
	Tno *tail;
	int nElementos;
}Cabecalho;


void incluiComeco(Tno *pLista, Cabecalho *L, int numero){
	Tno *pNovoNo;
	pNovoNo = (Tno *)malloc(sizeof(Tno));
	pNovoNo->valor = numero;
	if(pLista->prox == NULL){
		pLista->prox = pNovoNo;
		pLista->ant = pNovoNo;
		pNovoNo->prox = pNovoNo;
		pNovoNo->ant = pNovoNo;
		L->head = pNovoNo;
		L->tail = pNovoNo;
	}
	else if(pLista->prox != NULL){
		pNovoNo->ant = L->tail;
		L->tail->prox = pNovoNo;
		pNovoNo->prox = L->head;
		L->head->ant = pNovoNo;
		L->head = pNovoNo;
		pLista->prox = pNovoNo;
	}
	L->nElementos++;
}

void incluiFinal(Tno *pLista, Cabecalho *L, int numero){
	Tno *pNovoNo;
	pNovoNo = (Tno *)malloc(sizeof(Tno));
	pNovoNo->valor = numero;
	if(pLista->prox == NULL){
		pLista->prox = pNovoNo;
		pNovoNo->prox = pNovoNo;
		pNovoNo->ant = pNovoNo;
		L->head = pNovoNo;
		L->tail = pNovoNo;
		pLista->ant = pNovoNo;
	}
	else if(pLista->prox != NULL){
		pNovoNo->prox = L->head;
		L->head->ant = pNovoNo;
		pNovoNo->ant = L->tail;
		L->tail->prox = pNovoNo;
		L->tail = pNovoNo;
		pLista->ant = pNovoNo;
	}
	L->nElementos++;
}

void removeElementoPorChave(Tno *pLista, Cabecalho *L, int pChave){
	Tno *pAux, *p;
	int contador = 0;
	contador = L->nElementos;
	p = (Tno *)malloc(sizeof(Tno));
	pAux = pLista->prox;
	if(pAux != NULL){
		if(pChave == L->head->valor && pChave == L->tail->valor){
			p = pAux;
			pLista->prox = NULL;
			pLista->ant = NULL;
			L->head = NULL;
			L->tail = NULL;
			L->nElementos--;
			free(p);
		}
		else if(pChave == L->head->valor){
			p = L->head;
			L->head = L->head->prox;
			L->head->ant = L->tail;
			L->tail->prox = L->head;
			pLista->prox = L->head;
			L->nElementos--;
			free(p);
		}
		else if(pChave == L->tail->valor){
			p = L->tail;
			L->tail = L->tail->ant;
			L->tail->prox = L->head;
			L->head->ant = L->tail;
			pLista->ant = L->tail;
			L->nElementos--;
			free(p);
		}
		else{
			pAux = pAux->prox;
			while(pAux->valor != pChave && pAux != L->tail)
				pAux = pAux->prox;
			if(pAux->valor == pChave){
				p = pAux;
				pAux = pAux->ant;
				pAux->prox = p->prox;
				pAux->prox->ant = pAux;
				free(p);
				L->nElementos--;
			}
		}
	}
}
void removeElemento(Tno *pLista, Cabecalho *L, int numero){
	Tno *pAux, *p;
	p = (Tno *)malloc(sizeof(Tno));
	int i = 0;
	pAux = pLista;
	while(i < numero){
		pAux = pAux->prox;
		i++;
	}
	if(pAux != NULL){
		if(pAux->valor == L->head->valor && pAux->valor == L->tail->valor){
			p = pAux;
			pLista->prox = NULL;
			pLista->ant = NULL;
			L->head = NULL;
			L->tail = NULL;
			L->nElementos--;
			free(p);
		}
		else if(pAux->valor == L->head->valor){
			p = L->head;
			L->head = L->head->prox;
			L->head->ant = L->tail;
			L->tail->prox = L->head;
			pLista->prox = L->head;
			L->nElementos--;
			free(p);
		}
		else if(pAux->valor == L->tail->valor){
			p = L->tail;
			L->tail = L->tail->ant;
			L->tail->prox = L->head;
			L->head->ant = L->tail;
			pLista->ant = L->tail;
			L->nElementos--;
			free(p);
		}
		else{
			
			p = pAux;
			pAux = pAux->ant;
			pAux->prox = p->prox;
			pAux->prox->ant = pAux;
			free(p);
			L->nElementos--;
		}
	}
}

void alterarElemento(Tno *pLista, Cabecalho *L, int numero,int pChave){
	Tno *pAux;
	pAux = pLista->prox;
	if(L->tail->valor == pChave)
		L->tail->valor = numero;
	else{
		while(pAux != NULL && pAux->valor != pChave && pAux->valor != L->tail->valor)
			pAux = pAux->prox;
		if(pAux->valor == pChave)
			pAux->valor = numero;
	}
}

void mostrar(Tno *pLista, Cabecalho *L){
	Tno *pAux;
	pAux = pLista->prox;
	while(pAux != NULL && pAux != pLista->ant){
		printf("%d	",pAux->valor);
		pAux = pAux->prox;
	}
	if(pAux != NULL && pAux == pLista->ant){
		printf("%d	",pAux->valor);
	}
	printf("\n");
}

void mostrarInverso(Tno *pLista, Cabecalho *L){
	Tno *pAux;
	pAux = pLista->ant;
	while(pAux != NULL && pAux != pLista->prox){
		printf("%d	",pAux->valor);
		pAux = pAux->ant;
	}
	if(pAux != NULL && pAux == pLista->prox){
		printf("%d	",pAux->valor);
	}
	printf("\n");
}

int main(){
	char c[4], armazenaC;
	int numero, i = 0, cont = 0, pChave;
	Tno *pLista;
	Cabecalho *L;
	pLista = (Tno *)malloc(sizeof(Tno));
	L = (Cabecalho *)malloc(sizeof(Cabecalho));
	pLista->prox = NULL;
	pLista->ant = NULL;
	L->head = NULL;
	L->tail = NULL;
	L->nElementos = 0;
	
	do{
		i = 0;
		c[i] = getchar();
		i++;
		if(c[0] == 'I' || c[0] == 'i'){
			scanf("%d",&numero);
			incluiComeco(pLista, L, numero);
		}else if(c[0] == 'F' || c[0] == 'f'){
			scanf("%d",&numero);
			incluiFinal(pLista, L, numero);
		}else if(c[0] == 'R' || c[0] == 'r'){
			scanf("%d",&numero);
			//removeElementoPorChave(pLista, L, numero);
			if(numero <= L->nElementos && numero > 0){
				scanf("%d",&numero);
				removeElemento(pLista, L, numero);
			}
		}else if(c[0] == 'A' || c[0] == 'a'){
			scanf("%d",&pChave);
			scanf("%d",&numero);
			alterarElemento(pLista, L, numero, pChave);
		}else if(c[0] == 'L' || c[0] == 'l'){
			c[i] = getchar();
			if(c[1] == 'I' || c[1] == 'i'){
				mostrarInverso(pLista, L);
			}
			else if(c[1] == ' ' || c[1] == 10){
				mostrar(pLista, L);
			}
		}
	}while(c[0] != '*');
	
	return 0;
}
