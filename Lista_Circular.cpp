#include<stdio.h>
#include<stdlib.h>

typedef struct Tno{
	int valor;
	Tno *prox;
	Tno *ant;
}Tno;

typedef struct Cabecalho{
	Tno *head;
	Tno *tail;
	int nElementos;
}Cabecalho;


void incluiComeco(Tno *pLista, Cabecalho *L, int numero){
	Tno *pNovoNo;
	pNovoNo = (Tno *)malloc(sizeof(Tno));
	pNovoNo->valor = numero;
	if(pLista->prox == NULL){
		pLista->prox = pNovoNo;
		pLista->ant = pNovoNo;
		pNovoNo->prox = pNovoNo;
		pNovoNo->ant = pNovoNo;
		L->head = pNovoNo;
		L->tail = pNovoNo;
	}
	else if(pLista->prox != NULL){
		pNovoNo->ant = L->tail;
		pNovoNo->prox = L->head;
		L->head->ant = pNovoNo;
		L->head = pNovoNo;
		pLista->prox = L->head;
	}
	L->nElementos++;
}

void incluiFinal(Tno *pLista, Cabecalho *L, int numero){
	Tno *pNovoNo;
	pNovoNo = (Tno *)malloc(sizeof(Tno));
	pNovoNo->valor = numero;
	if(pLista->prox == NULL){
		pLista->prox = pNovoNo;
		pLista->ant = pNovoNo;
		pNovoNo->prox = pNovoNo;
		pNovoNo->ant = pNovoNo;
		L->head = pNovoNo;
		L->tail = pNovoNo;
	}
	else if(pLista->prox != NULL){
		pNovoNo->prox = L->head;
		pNovoNo->ant = L->tail;
		L->tail->prox = pNovoNo;
		L->tail = pNovoNo;
		pLista->ant = L->tail;
	}
	L->nElementos++;
}

void mostrar(Tno *pLista, Cabecalho *L){
	Tno *pAux;
	pAux = pLista->prox;
	while(pAux != NULL && pAux != pLista->ant){
		printf("%d	",pAux->valor);
		pAux = pAux->prox;
	}
	if(pAux != NULL && pAux == pLista->ant){
		printf("%d	",pAux->valor);
	}
	printf("\n");
}

int main(){
	char c;
	int numero;
	Tno *pLista;
	Cabecalho *L;
	pLista = (Tno *)malloc(sizeof(Tno));
	L = (Cabecalho *)malloc(sizeof(Cabecalho));
	pLista->prox = NULL;
	pLista->ant = NULL;
	L->head = NULL;
	L->tail = NULL;
	L->nElementos = 0;
	
	do{
		scanf("%c",&c);
		switch(c){
			case 'I':
			case 'i':
				scanf("%d",&numero);
				incluiComeco(pLista, L, numero);
				break;
			case 'F':
			case 'f':
				scanf("%d",&numero);
				incluiFinal(pLista, L, numero);
				break;
			case 'L':
			case 'l':
				mostrar(pLista, L);
				break;
			}
		}while(c != '*');
	
	return 0;
}
