#include<stdio.h>
#include<stdlib.h>

typedef struct Tno{
	int valor;
	Tno *prox;
	Tno *ant;
}Tno;

typedef struct Cabecalho{
	Tno *head;
	Tno *tail;
	int nElementos;
}Cabecalho;


void incluiComeco(Tno *pLista, Cabecalho *L, int numero){
	Tno *pNovoNo;
	pNovoNo = (Tno *)malloc(sizeof(Tno));
	pNovoNo->valor = numero;
	if(pLista->prox == NULL){
		pLista->prox = pNovoNo;
		pLista->ant = pNovoNo;
		pNovoNo->prox = pNovoNo;
		pNovoNo->ant = pNovoNo;
		L->head = pNovoNo;
		L->tail = pNovoNo;
	}
	else if(pLista->prox != NULL){
		pNovoNo->ant = L->tail;
		pNovoNo->prox = L->head;
		L->head->ant = pNovoNo;
		L->head = pNovoNo;
		pLista->prox = L->head;
	}
	L->nElementos++;
}

void incluiFinal(Tno *pLista, Cabecalho *L, int numero){
	Tno *pNovoNo;
	pNovoNo = (Tno *)malloc(sizeof(Tno));
	pNovoNo->valor = numero;
	if(pLista->prox == NULL){
		pLista->prox = pNovoNo;
		pLista->ant = pNovoNo;
		pNovoNo->prox = pNovoNo;
		pNovoNo->ant = pNovoNo;
		L->head = pNovoNo;
		L->tail = pNovoNo;
	}
	else if(pLista->prox != NULL){
		pNovoNo->prox = L->head;
		pNovoNo->ant = L->tail;
		L->tail->prox = pNovoNo;
		L->tail = pNovoNo;
		pLista->ant = L->tail;
	}
	L->nElementos++;
}

void mostrar(Tno *pLista, Cabecalho *L){
	Tno *pAux;
	pAux = pLista->prox;
	while(pAux != NULL && pAux != pLista->ant){
		printf("%d	",pAux->valor);
		pAux = pAux->prox;
	}
	if(pAux != NULL && pAux == pLista->ant){
		printf("%d	",pAux->valor);
	}
	printf("\n");
}

void troca(Tno *pNoMaior,Tno *pNoMenor){
	Tno *pAux;
	pAux = (Tno *)malloc(sizeof(Tno));
	pAux->valor = pNoMaior->valor;
	pAux->ant = pNoMaior->ant;
	pNoMenor->ant = pAux->ant;
	pAux->prox = pNoMenor->prox;
	pNoMenor->prox = pAux;
	pAux->ant = pNoMenor;
	free(pNoMaior);
}

void ordenaLista(Tno *pLista, Cabecalho *L){
	Tno *pAux;
	pAux = L->head;
	Tno *maior;
	maior = pAux;
	do{
		if(pAux->valor > maior->valor){
			troca(pAux, maior);
			maior = pAux;
		}
		
		pAux=pAux->prox;
	}while(pAux != L->head);
	
		mostrar(pLista, L);
}

int main(){
	int numero;
	Tno *pLista;
	Cabecalho *L;
	pLista = (Tno *)malloc(sizeof(Tno));
	L = (Cabecalho *)malloc(sizeof(Cabecalho));
	pLista->prox = NULL;
	pLista->ant = NULL;
	L->head = NULL;
	L->tail = NULL;
	L->nElementos = 0;
	
	do{
		scanf("%d",&numero);
		if(numero != 0)
			incluiFinal(pLista, L, numero);
		}while(numero != 0);
	mostrar(pLista, L);
	return 0;
}
